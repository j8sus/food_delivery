Rails.application.routes.draw do
  devise_for :users
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :places, only: [:index, :show]
  resources :users, only: [:show, :edit, :update]
  resources :orders, only: [:index, :show, :create, :destroy]
  
  root 'places#index'
  post 'cart/add' => 'cart#add', :as => 'cart_add'
  post 'cart/minus' => 'cart#minus', :as => 'cart_minus'
  post 'cart/plus' => 'cart#plus', :as => 'cart_plus'
  post 'cart/remove' => 'cart#remove', :as => 'cart_remove'
  post 'cart/clear' => 'cart#clear', :as => 'cart_clear'
end