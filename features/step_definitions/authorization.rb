When(/^регистрируется новый пользователь с email "([^"]*)" и паролем "([^"]*)"$/) do |email, password|
	visit('/users/sign_up')
	within("#new_user") do
		fill_in('Email', with: email)
		fill_in('Password', with: password)
		fill_in('Password confirmation', with: password)
		click_button 'Sign up'
	end
end

Then(/^он попробует перейти на страницу админ панели$/) do
  visit('/admin')
end

Then(/^система не даст ему это сделать$/) do
  expect(page).to have_content 'You are not authorized to perform this action.'
end