When(/^залогинен пользователь с email "([^"]*)" и паролем "([^"]*)"$/) do |email, password|
  visit('/users/sign_in')
	within("#new_user") do
		fill_in('Email', with: email)
		fill_in('Password', with: password)
		click_button 'Log in'
	end
end

Then(/^он перейдёт на страницу ресторана$/) do
  visit('/places/1')
end

Then(/^он добавит блюдо из ресторана$/) do
  click_link('item-1')
end

Then(/^он произведёт заказ$/) do
  click_on('Checkout')
end

Then(/^заказ появится у него в панели заказов$/) do
  expect(page).to have_content 'Order was created.'
end