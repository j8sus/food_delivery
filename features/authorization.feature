# language: ru

Функционал: Не админ не может войти в админ панель
	Как обычный пользователь
	Я не должен получить доступ к панели
	Чтобы не нарушить безопасность приложения

@javascript
Сценарий: Заходит обычный пользователь
	Допустим регистрируется новый пользователь с email "mortal@user.su" и паролем "qwerty"
	Если он попробует перейти на страницу админ панели
	То система не даст ему это сделать