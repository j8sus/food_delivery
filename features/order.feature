# language: ru

Функционал: Заказ блюд лежащих в корзине
	Как покупатель
	Я хочу иметь возможность добавить товар в корзину
	И сделать заказ

@javascript
Сценарий: Заказ
	Допустим залогинен пользователь с email "alex@g.dot" и паролем "qwerty"
	Если он перейдёт на страницу ресторана
	Если он добавит блюдо из ресторана
	Если он произведёт заказ
	То заказ появится у него в панели заказов