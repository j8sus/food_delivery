admin = User.create!(name: 'Admin', email: 'admin@admin.com', password: 'qwerty', role: 'admin')
user = User.create!(name: 'Alex', email: 'alex@g.dot', password: 'qwerty')

about_place = 'This place has choice of healthy food. Price is surprisingly cheap.'
about_dish = 'Very tasty. Delicious central asian traditional dish'

place = Place.create!(name: 'Cafe Faiza', about: about_place)

place.items.create!(name: 'Lagman', about: about_dish, price: 100)
place.items.create!(name: 'Beshbarmak', about: about_dish, price: 120)
place.items.create!(name: 'Plov', about: about_dish, price: 150)
place.items.create!(name: 'Manty', about: about_dish, price: 130)
place.items.create!(name: 'Shashlyk', about: about_dish, price: 140)

about_place = 'Sierra is about experience. Coffee, smoothies, brownies, paninis, soups, salads, music and service are all part of bringing you a great experience. Whether you’ve had a tasty drink, time with your friends or just a few hours to work on your computer – we are happy if you had a good experience at one of our cafes!'
about_dish = 'Awaken Your Senses'

place = Place.create!(name: 'Sierra Coffee', about: about_place)

place.items.create!(name: 'Americano', about: about_dish, price: 120)
place.items.create!(name: 'Cappuccino', about: about_dish, price: 140)
place.items.create!(name: 'Latte', about: about_dish, price: 150)
place.items.create!(name: 'Espresso', about: about_dish, price: 150)
place.items.create!(name: 'Irish', about: about_dish, price: 180)