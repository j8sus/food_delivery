class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.references :place, foreign_key: true
      t.string :name
      t.text :about
      t.decimal :price

      t.timestamps
    end
  end
end
