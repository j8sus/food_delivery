class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.references :order, foreign_key: true
      t.belongs_to :item, index: true, foreign_key: true
      t.integer :count

      t.timestamps
    end
  end
end
