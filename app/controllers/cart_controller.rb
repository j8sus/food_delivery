class CartController < ApplicationController
	before_action :authenticate_user!

	def add
		place = params[:place]
		item = params[:item]
		count = params[:count].to_i

		user_session[place] ||= {}

		user_session[place][item] = count

		flash[:notice] = "Items was added"
		redirect_to place_path(place)
	end

	def minus
		place = params[:place]
		item = params[:item]

		if user_session[place][item] > 1
			user_session[place][item] -= 1
			flash[:notice] = "Count was changed."
		else
			flash[:alert] = "Items in cart must be more than 1. "
		end
		redirect_to place_path(place)
	end

	def plus
		place = params[:place]
		item = params[:item]

		if user_session[place][item] < 10
			user_session[place][item] += 1
			flash[:notice] = "Count was changed."
		else
			flash[:alert] = "Items in cart must be less than 10. "
		end
		redirect_to place_path(place)
	end

	def remove
		place = params[:place]
		item = params[:item]

		user_session[place].delete(item)
		flash[:notice] = "Item was deleted."

		redirect_to place_path(place)		
	end

	def clear
		place = params[:place]
		user_session[place] = {}	

		flash[:alert] = "Cart was cleared."
		redirect_to place_path(place)
	end
end