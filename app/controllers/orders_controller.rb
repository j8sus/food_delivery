class OrdersController < ApplicationController
	before_action :authenticate_user!

	def index
		@orders = User.find(current_user).orders
	end

	def show
		@order = Order.find(params[:id])	
	end

	def create
		user = current_user
		place = params[:place]
		total = params[:total]
		order = Order.create(user: user, total: total)		

		user_session[place].each do |item, count|
			order.order_items.create(item_id: item.to_i, count: count)
		end

		user_session[place] = {}
		flash[:notice] = "Order was created."
		redirect_to order
	end

	def destroy
		Order.destroy(params[:id])
		flash[:notice] = "Order was destroyed."
		redirect_to orders_path
	end
end