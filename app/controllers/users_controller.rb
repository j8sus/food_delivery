class UsersController < ApplicationController
	before_action :authenticate_user!, :true_user, only: [:edit, :update]

	def show
		@user = User.find(current_user)
	end

	def edit
		@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])

		if @user.update(user_params)
			redirect_to user_path(@user)
			flash[:notice] = "Information successfully updated"	
		else
			render 'edit'
		end
	end

	private

	def true_user
		redirect_to edit_user_path(current_user) if current_user.id.to_s != params[:id]
	end

	def user_params
		params.require(:user).permit(:name, :phone, :address)	
	end
end