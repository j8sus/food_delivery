ActiveAdmin.register Item do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :about, :price, :place_id

index do
		selectable_column
		id_column
		column :name do |item|
			link_to item.name, admin_item_path(item)
		end
		column :place_id do |item|
			link_to item.place.name, admin_place_path(item.place)
		end
		actions
	end

end
